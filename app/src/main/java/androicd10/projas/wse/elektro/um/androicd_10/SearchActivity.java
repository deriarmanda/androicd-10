package androicd10.projas.wse.elektro.um.androicd_10;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androicd10.projas.wse.elektro.um.androicd_10.pojo.IndeksPenyakit;
import androicd10.projas.wse.elektro.um.androicd_10.pojo.KamusPenyakit;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.ListPenyakitRvAdapter;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.PenyakitRvOnItemClickListener;
import androicd10.projas.wse.elektro.um.androicd_10.utility.MyDatabaseHelper;

public class SearchActivity extends AppCompatActivity {

    EditText mSearchBox;
    RecyclerView mRvResult;
    Spinner mSpinner;
    ImageButton mBackBtn;

    private ListPenyakitRvAdapter rvAdapter;
    private List<KamusPenyakit> listPenyakit;
    private MyDatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        databaseHelper = MyDatabaseHelper.getInstance(this);
        listPenyakit = databaseHelper.getListPenyakit();

        mSpinner = (Spinner) findViewById(R.id.search_spinner);
        mRvResult = (RecyclerView) findViewById(R.id.search_rv);
        PenyakitRvOnItemClickListener clickListener = new PenyakitRvOnItemClickListener() {
            @Override
            public void onRecyclerItemClick(IndeksPenyakit indeksPenyakit) {

            }

            @Override
            public void onRecyclerSubItemClick(KamusPenyakit kamusPenyakit, int clickMode) {
                switch (clickMode) {
                    case PenyakitRvOnItemClickListener.SUB_ITEM_MODE_DETAIL:
                        Intent detailIntent = new Intent(SearchActivity.this, DetailActivity.class);
                        detailIntent.putExtra("indeks_list", databaseHelper.getListPenyakit().indexOf(kamusPenyakit));
                        startActivity(detailIntent);
                        break;
                    case PenyakitRvOnItemClickListener.SUB_ITEM_MODE_EDIT:
                        Intent editIntent = new Intent(SearchActivity.this, UpsertActivity.class);
                        editIntent.putExtra("indeks_list", databaseHelper.getListPenyakit().indexOf(kamusPenyakit));
                        startActivity(editIntent);
                        break;
                    case PenyakitRvOnItemClickListener.SUB_ITEM_MODE_BOOKMARK:
                        if (kamusPenyakit.bookmark == 0) {
                            databaseHelper.removeBookmark(kamusPenyakit);
                            Toast.makeText(SearchActivity.this, kamusPenyakit.namaPenyakit
                                    + " dihapus dari bookmark.", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            databaseHelper.addBookmark(kamusPenyakit);
                            Toast.makeText(SearchActivity.this, kamusPenyakit.namaPenyakit
                                    + " ditambahkan ke bookmark.", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        break;
                }
            }
        };
        rvAdapter = new ListPenyakitRvAdapter(new ArrayList<KamusPenyakit>(), clickListener);
        mRvResult.setAdapter(rvAdapter);
        mRvResult.setLayoutManager(new LinearLayoutManager(this));

        mSearchBox = (EditText) findViewById(R.id.search_box);
        mSearchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int mode = mSpinner.getSelectedItemPosition() + 1;
                rvAdapter.onQuerySearch(listPenyakit, s+"", mode, true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mBackBtn = (ImageButton) findViewById(R.id.search_back_btn);
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
