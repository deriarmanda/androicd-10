package androicd10.projas.wse.elektro.um.androicd_10.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import androicd10.projas.wse.elektro.um.androicd_10.DetailActivity;
import androicd10.projas.wse.elektro.um.androicd_10.ListPenyakitActivity;
import androicd10.projas.wse.elektro.um.androicd_10.R;
import androicd10.projas.wse.elektro.um.androicd_10.pojo.IndeksPenyakit;
import androicd10.projas.wse.elektro.um.androicd_10.pojo.KamusPenyakit;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.FragmentKodeRvAdapter;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.PenyakitRvOnItemClickListener;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.PenyakitRvOnScrollListener;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.PenyakitRvItemDecoration;
import androicd10.projas.wse.elektro.um.androicd_10.utility.MyDatabaseHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class KodePenyakitFragment extends Fragment {

    RecyclerView rvListIndeks;
    LinearLayoutManager rvListLayoutManager;

    ArrayList<IndeksPenyakit> listIndeksPenyakit;
    FragmentKodeRvAdapter rvListAdapter;
    PenyakitRvOnScrollListener indeksScrollListener;
    PenyakitRvOnItemClickListener clickListener;
    public KodePenyakitFragment() {
        // Required empty public constructor
    }

    public KodePenyakitFragment(PenyakitRvOnScrollListener indeksScrollListener) {
        this.indeksScrollListener = indeksScrollListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nama_penyakit, container, false);

        listIndeksPenyakit = MyDatabaseHelper.getInstance(getContext()).getIndeksKodePenyakit();

        clickListener = new PenyakitRvOnItemClickListener() {
            @Override
            public void onRecyclerItemClick(IndeksPenyakit indeksPenyakit) {
                Intent intent = new Intent(getContext(), ListPenyakitActivity.class);
                intent.putExtra("indeks_penyakit", indeksPenyakit);
                startActivity(intent);
            }

            @Override
            public void onRecyclerSubItemClick(KamusPenyakit kamusPenyakit, int clickMode) {
                Intent intent = new Intent(getContext(), DetailActivity.class);
                intent.putExtra("kamus_penyakit", kamusPenyakit);
                startActivity(intent);
            }
        };
        String[] indeksSubtitle = getResources().getStringArray(R.array.indeks_kode_subtitle);
        rvListAdapter = new FragmentKodeRvAdapter(listIndeksPenyakit, indeksSubtitle, clickListener);
        rvListIndeks = (RecyclerView) view;
        rvListIndeks.setAdapter(rvListAdapter);
        rvListIndeks.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (indeksScrollListener != null) indeksScrollListener
                        .onRecyclerViewScrollStateChanged(recyclerView, newState);
            }
        });
        rvListIndeks.setLayoutManager(new LinearLayoutManager(getContext()));
        rvListIndeks.addItemDecoration(new PenyakitRvItemDecoration(true, dpToPx(0), dpToPx(0), dpToPx(0), dpToPx(2)));

        return view;
    }

    private int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
