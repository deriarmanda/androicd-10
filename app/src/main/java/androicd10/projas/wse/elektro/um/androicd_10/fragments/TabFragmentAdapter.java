package androicd10.projas.wse.elektro.um.androicd_10.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.PenyakitRvOnScrollListener;

/**
 * Created by DERI on 29/10/2017.
 */

public class TabFragmentAdapter extends FragmentPagerAdapter {
    private String[] title;
    private PenyakitRvOnScrollListener onRecyclerScrollListener;

    public TabFragmentAdapter(FragmentManager fm, PenyakitRvOnScrollListener onRecyclerScrollListener, String... title) {
        super(fm); this.title = title;
        this.onRecyclerScrollListener = onRecyclerScrollListener;
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new BookmarkFragment(onRecyclerScrollListener);
            case 1:
                return new NamaPenyakitFragment(onRecyclerScrollListener);
            case 2:
                return new KodePenyakitFragment(onRecyclerScrollListener);
            default:
                return null;
        }
    }
}
