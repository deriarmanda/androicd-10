package androicd10.projas.wse.elektro.um.androicd_10.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import androicd10.projas.wse.elektro.um.androicd_10.DetailActivity;
import androicd10.projas.wse.elektro.um.androicd_10.ListPenyakitActivity;
import androicd10.projas.wse.elektro.um.androicd_10.R;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.FragmentNamaRvAdapter;
import androicd10.projas.wse.elektro.um.androicd_10.pojo.IndeksPenyakit;
import androicd10.projas.wse.elektro.um.androicd_10.pojo.KamusPenyakit;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.PenyakitRvOnItemClickListener;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.PenyakitRvOnScrollListener;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.PenyakitRvItemDecoration;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.GridAutoFitLayoutManager;
import androicd10.projas.wse.elektro.um.androicd_10.utility.MyDatabaseHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class NamaPenyakitFragment extends Fragment {

    RecyclerView rvGridIndeks;
    GridLayoutManager rvGridLayoutManager;

    ArrayList<IndeksPenyakit> listIndeksPenyakit;
    FragmentNamaRvAdapter rvGridAdapter;
    PenyakitRvOnScrollListener indeksScrollListener;
    PenyakitRvOnItemClickListener clickListener;

    public NamaPenyakitFragment() {
        // Required empty public constructor
    }

    public NamaPenyakitFragment(PenyakitRvOnScrollListener indeksScrollListener) {
        this.indeksScrollListener = indeksScrollListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nama_penyakit, container, false);

        listIndeksPenyakit = MyDatabaseHelper.getInstance(getContext()).getIndeksNamaPenyakit();

        // Initialize list
        clickListener = new PenyakitRvOnItemClickListener() {
            @Override
            public void onRecyclerItemClick(IndeksPenyakit indeksPenyakit) {
                Intent intent = new Intent(getContext(), ListPenyakitActivity.class);
                intent.putExtra("indeks_penyakit", indeksPenyakit);
                startActivity(intent);
            }

            @Override
            public void onRecyclerSubItemClick(KamusPenyakit kamusPenyakit, int clickMode) {
                Intent intent = new Intent(getContext(), DetailActivity.class);
                intent.putExtra("kamus_penyakit", kamusPenyakit);
                startActivity(intent);
            }
        };
        rvGridAdapter = new FragmentNamaRvAdapter(listIndeksPenyakit, clickListener);
        rvGridIndeks = (RecyclerView) view;
        rvGridIndeks.setAdapter(rvGridAdapter);
        rvGridIndeks.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (indeksScrollListener != null) indeksScrollListener
                        .onRecyclerViewScrollStateChanged(recyclerView, newState);
            }
        });
        int columns = calculateNoOfColumns(dpToPx(128));
        rvGridLayoutManager = new GridLayoutManager(getContext(), columns);
        rvGridIndeks.setLayoutManager(new GridAutoFitLayoutManager(getContext(), dpToPx(128) + dpToPx(columns*8)));
        rvGridIndeks.addItemDecoration(new PenyakitRvItemDecoration(dpToPx(8), true));

        return view;
    }

    private int calculateNoOfColumns(int itemWidth) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        return (int) (dpWidth / itemWidth);
    }

    private int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
