package androicd10.projas.wse.elektro.um.androicd_10.recyclerviews;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.List;

import androicd10.projas.wse.elektro.um.androicd_10.R;
import androicd10.projas.wse.elektro.um.androicd_10.pojo.IndeksPenyakit;

/**
 * Created by DERI on 18/11/2017.
 */

public class FragmentNamaRvAdapter extends RecyclerView.Adapter {

    private final ColorGenerator colorGenerator = ColorGenerator.MATERIAL;

    private List<IndeksPenyakit> mListIndeksNama;
    private PenyakitRvOnItemClickListener mClickListener;

    public FragmentNamaRvAdapter(List<IndeksPenyakit> listIndeksNama,
                                 PenyakitRvOnItemClickListener clickListener) {
        mListIndeksNama = listIndeksNama;
        mClickListener = clickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View indeks = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item_nama_penyakit, parent, false);
        return new IndeksGridVH(indeks);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        IndeksGridVH indeksVH = (IndeksGridVH) holder;
        final IndeksPenyakit curIndeks = mListIndeksNama.get(position);

        int color = colorGenerator.getColor(curIndeks.getIndeksTitle());
        indeksVH.indeksCard.setCardBackgroundColor(color);
        indeksVH.indeksTitle.setTextColor(color);
        indeksVH.indeksTitle.setText(curIndeks.getIndeksTitle());

        indeksVH.indeksContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) mClickListener
                        .onRecyclerItemClick(curIndeks);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListIndeksNama.size();
    }

    private class IndeksGridVH extends RecyclerView.ViewHolder {

        CardView indeksCard;
        LinearLayout indeksContainer;
        TextView indeksTitle;

        IndeksGridVH(View itemView) {
            super(itemView);
            indeksCard = (CardView) itemView;
            indeksContainer = (LinearLayout) indeksCard.findViewById(R.id.rv_item_nama_indeks_container);
            indeksTitle = (TextView) indeksCard.findViewById(R.id.rv_item_nama_indeks_title);
        }
    }
}
