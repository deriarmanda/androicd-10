package androicd10.projas.wse.elektro.um.androicd_10.recyclerviews;

import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by DERI on 17/11/2017.
 */

public class PenyakitRvItemDecoration extends RecyclerView.ItemDecoration {
    private final boolean includeEdge;
    private int spacing, spacingLeft, spacingRight, spacingTop, spacingBottom;


    public PenyakitRvItemDecoration(int spacing, boolean includeEdge) {
        spacingLeft = spacing; spacingRight = spacing;
        spacingTop = spacing; spacingBottom = spacing;
        this.spacing = spacing;
        this.includeEdge = includeEdge;
    }

    public PenyakitRvItemDecoration(boolean includeEdge, int spacingLeft,
                                    int spacingRight, int spacingTop, int spacingBottom) {
        this.includeEdge = includeEdge;
        this.spacingLeft = spacingLeft;
        this.spacingRight = spacingRight;
        this.spacingTop = spacingTop;
        this.spacingBottom = spacingBottom;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (parent.getLayoutManager() instanceof GridLayoutManager) {
            GridLayoutManager layoutManager = (GridLayoutManager)parent.getLayoutManager();
            int spanCount = layoutManager.getSpanCount();
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }

        } else {
            int position = parent.getChildAdapterPosition(view); // item position
            outRect.left = spacingLeft; outRect.right = spacingRight;
            if (position > 0) outRect.top = spacingTop;
            outRect.bottom = spacingBottom;
        }

    }
}
