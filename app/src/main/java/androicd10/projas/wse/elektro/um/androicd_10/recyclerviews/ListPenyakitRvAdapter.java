package androicd10.projas.wse.elektro.um.androicd_10.recyclerviews;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.Date;
import java.util.List;

import androicd10.projas.wse.elektro.um.androicd_10.R;
import androicd10.projas.wse.elektro.um.androicd_10.pojo.KamusPenyakit;

/**
 * Created by DERI on 19/11/2017.
 */

public class ListPenyakitRvAdapter extends RecyclerView.Adapter {

    private final int SEARCH_BOTH = 1, SEARCH_NAMA = 2, SEARCH_KODE = 3;
    private final ColorGenerator colorGenerator = ColorGenerator.MATERIAL;

    private List<KamusPenyakit> mListPenyakit;
    private PenyakitRvOnItemClickListener mOnItemClickListener;

    public ListPenyakitRvAdapter(List<KamusPenyakit> mListPenyakit,
                                 PenyakitRvOnItemClickListener mOnItemClickListener) {
        this.mListPenyakit = mListPenyakit;
        this.mOnItemClickListener = mOnItemClickListener;
        for (KamusPenyakit kp : mListPenyakit)
            Log.d("ListPenyakitActivity", kp.namaPenyakit);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item_list_penyakit, parent, false);
        return new ListPenyakitVH(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final KamusPenyakit penyakit = mListPenyakit.get(position);
        final ListPenyakitVH penyakitVH = (ListPenyakitVH) holder;

        int color = colorGenerator.getRandomColor();
        penyakitVH.kodeTv.setBackgroundColor(color);
        penyakitVH.kodeTv.setText(penyakit.kodePenyakit);
        penyakitVH.titleTv.setText(penyakit.namaPenyakit);

        penyakitVH.penjelasanTv.setText(penyakit.penjelasan);
        int bookmarkRes = penyakit.isBookmarked()? R.drawable.ic_bookmark_accent_24dp
                : R.drawable.ic_bookmark_border_accent_24dp;
        penyakitVH.bookmarkBtn.setImageResource(bookmarkRes);

        penyakitVH.bookmarkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (penyakit.isBookmarked()) {
                    penyakit.bookmark = 0;
                    penyakitVH.bookmarkBtn.animate().rotation(-360).start();
                    penyakitVH.bookmarkBtn.setImageResource(R.drawable.ic_bookmark_border_accent_24dp);
                } else {
                    penyakit.bookmark = new Date().getTime();
                    penyakitVH.bookmarkBtn.animate().rotation(360).start();
                    penyakitVH.bookmarkBtn.setImageResource(R.drawable.ic_bookmark_accent_24dp);
                }

                if (mOnItemClickListener != null) mOnItemClickListener
                        .onRecyclerSubItemClick(penyakit, PenyakitRvOnItemClickListener.SUB_ITEM_MODE_BOOKMARK);
            }
        });
        penyakitVH.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) mOnItemClickListener
                        .onRecyclerSubItemClick(penyakit, PenyakitRvOnItemClickListener.SUB_ITEM_MODE_EDIT);
            }
        });
        penyakitVH.moreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) mOnItemClickListener
                        .onRecyclerSubItemClick(penyakit, PenyakitRvOnItemClickListener.SUB_ITEM_MODE_DETAIL);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListPenyakit.size();
    }

    public void onQuerySearch(List<KamusPenyakit> listCopy, String query, int mode, boolean clear) {
        mListPenyakit.clear();
        if (query.length() == 0 && !clear) {
            mListPenyakit.addAll(listCopy);
            Log.d("ListPenyakitActivity", "Query kosong, size: " + mListPenyakit.size());
        }
        else {
            query = query.toLowerCase();
            for (KamusPenyakit kp : listCopy) {
                if (mode == SEARCH_NAMA) {
                    if (kp.namaPenyakit.toLowerCase().contains(query)) mListPenyakit.add(kp);
                } else if (mode == SEARCH_KODE) {
                    if (kp.kodePenyakit.toLowerCase().contains(query)) mListPenyakit.add(kp);
                } else {
                    if (kp.namaPenyakit.toLowerCase().contains(query) ||
                            kp.kodePenyakit.toLowerCase().contains(query)) mListPenyakit.add(kp);
                }
            }
            Log.d("ListPenyakitActivity", "Query submit, size: " + mListPenyakit.size());
        } notifyDataSetChanged();
    }

    private class ListPenyakitVH extends RecyclerView.ViewHolder {

        TextView titleTv, kodeTv, penjelasanTv;
        ImageButton bookmarkBtn, editBtn;
        Button moreBtn;

        ListPenyakitVH(View itemView) {
            super(itemView);
            kodeTv = (TextView) itemView.findViewById(R.id.rv_item_list_kode);
            titleTv = (TextView) itemView.findViewById(R.id.rv_item_list_title);
            penjelasanTv = (TextView) itemView.findViewById(R.id.rv_item_list_content);
            bookmarkBtn = (ImageButton) itemView.findViewById(R.id.rv_item_list_bookmark_btn);
            editBtn = (ImageButton) itemView.findViewById(R.id.rv_item_list_edit_btn);
            moreBtn = (Button) itemView.findViewById(R.id.rv_item_list_more_btn);
        }
    }
}
