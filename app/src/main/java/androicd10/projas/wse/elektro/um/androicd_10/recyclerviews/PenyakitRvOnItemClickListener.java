package androicd10.projas.wse.elektro.um.androicd_10.recyclerviews;

import androicd10.projas.wse.elektro.um.androicd_10.pojo.IndeksPenyakit;
import androicd10.projas.wse.elektro.um.androicd_10.pojo.KamusPenyakit;

/**
 * Created by DERI on 17/11/2017.
 */

public interface PenyakitRvOnItemClickListener {

    int SUB_ITEM_MODE_DETAIL = -1, SUB_ITEM_MODE_EDIT = -2, SUB_ITEM_MODE_BOOKMARK = -3;

    void onRecyclerItemClick (IndeksPenyakit indeksPenyakit);
    void onRecyclerSubItemClick (KamusPenyakit kamusPenyakit, int clickMode);
}
