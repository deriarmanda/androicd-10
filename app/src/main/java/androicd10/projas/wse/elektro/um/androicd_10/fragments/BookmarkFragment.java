package androicd10.projas.wse.elektro.um.androicd_10.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import androicd10.projas.wse.elektro.um.androicd_10.DetailActivity;
import androicd10.projas.wse.elektro.um.androicd_10.ListPenyakitActivity;
import androicd10.projas.wse.elektro.um.androicd_10.R;
import androicd10.projas.wse.elektro.um.androicd_10.pojo.IndeksPenyakit;
import androicd10.projas.wse.elektro.um.androicd_10.pojo.KamusPenyakit;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.FragmentBookmarkRvAdapter;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.PenyakitRvOnItemClickListener;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.PenyakitRvOnScrollListener;
import androicd10.projas.wse.elektro.um.androicd_10.utility.MyDatabaseHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookmarkFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout mRefreshLayout;
    RecyclerView mBookmarkRv;
    LinearLayoutManager mBookmarkRvLayoutManager;
    FragmentBookmarkRvAdapter mBookmarkRvAdapter;
    PenyakitRvOnScrollListener mBookmarkRvScrollListener;
    PenyakitRvOnItemClickListener mBookmarkRvItemClickListener;

    MyDatabaseHelper databaseHelper;

    public BookmarkFragment() {
        // Required empty public constructor
    }

    public BookmarkFragment(PenyakitRvOnScrollListener mBookmarkRvScrollListener) {
        this.mBookmarkRvScrollListener = mBookmarkRvScrollListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        databaseHelper = MyDatabaseHelper.getInstance(getContext());

        mRefreshLayout = (SwipeRefreshLayout) inflater.inflate(R.layout.fragment_bookmark, container, false);
        mRefreshLayout.setNestedScrollingEnabled(true);
        mRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mRefreshLayout.setOnRefreshListener(this);

        mBookmarkRv = (RecyclerView) mRefreshLayout.findViewById(R.id.bookmark_rv);
        mBookmarkRvLayoutManager = new LinearLayoutManager(getContext());
        mBookmarkRv.setLayoutManager(mBookmarkRvLayoutManager);
        mBookmarkRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (mBookmarkRvScrollListener != null) mBookmarkRvScrollListener
                        .onRecyclerViewScrollStateChanged(recyclerView, newState);
            }
        });
        mBookmarkRvItemClickListener = new PenyakitRvOnItemClickListener() {
            @Override
            public void onRecyclerItemClick(IndeksPenyakit indeksPenyakit) {
                Intent intent = new Intent(getContext(), ListPenyakitActivity.class);
                intent.putExtra("indeks_penyakit", indeksPenyakit);
                startActivity(intent);
            }

            @Override
            public void onRecyclerSubItemClick(final KamusPenyakit kamusPenyakit, int clickMode) {
                if (clickMode == PenyakitRvOnItemClickListener.SUB_ITEM_MODE_DETAIL) {
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra("indeks_list", databaseHelper.getListPenyakit().indexOf(kamusPenyakit));
                    startActivity(intent);
                } else {
                    kamusPenyakit.bookmark = 0;
                    databaseHelper.removeBookmark(kamusPenyakit);
                    Toast.makeText(getContext(), kamusPenyakit.namaPenyakit
                            + " dihapus dari bookmark.", Toast.LENGTH_SHORT).show();
                }
            }
        };

        ArrayList<KamusPenyakit> listBookmarks = new ArrayList<>();
        for (Integer i : databaseHelper.getListBookmark()) {
            listBookmarks.add(databaseHelper.getListPenyakit().get(i));
        }

        mBookmarkRvAdapter = new FragmentBookmarkRvAdapter(listBookmarks, mBookmarkRvItemClickListener);
        mBookmarkRv.setAdapter(mBookmarkRvAdapter);

        return mRefreshLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        onRefresh();
    }

    @Override
    public void onRefresh() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                ArrayList<KamusPenyakit> newLists = new ArrayList<>();
                for (Integer i : databaseHelper.getListBookmark()) {
                    newLists.add(databaseHelper.getListPenyakit().get(i));
                }
                mBookmarkRvAdapter.refreshList(newLists);
                mRefreshLayout.setRefreshing(false);
            }
        });
    }
}
