package androicd10.projas.wse.elektro.um.androicd_10.recyclerviews;

import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import androicd10.projas.wse.elektro.um.androicd_10.R;
import androicd10.projas.wse.elektro.um.androicd_10.pojo.KamusPenyakit;

/**
 * Created by DERI on 23/11/2017.
 */

public class FragmentBookmarkRvAdapter extends RecyclerView.Adapter {

    private final int VIEW_TYPE_HEADER = 1, VIEW_TYPE_ITEM = 2;
    private final ColorGenerator colorGenerator = ColorGenerator.MATERIAL;

    private ArrayList<KamusPenyakit> mListBookmarks;
    private PenyakitRvOnItemClickListener mClickListener;

    public FragmentBookmarkRvAdapter(ArrayList<KamusPenyakit> mListBookmarks, PenyakitRvOnItemClickListener mClickListener) {
        this.mListBookmarks = mListBookmarks;
        this.mClickListener = mClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return VIEW_TYPE_HEADER;
        else return VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder;
        View view;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                    view = LayoutInflater
                            .from(parent.getContext()).inflate(R.layout.rv_header_bookmark, parent, false);
                    holder = new HeaderBookmarkVH(view);
                break;
            case VIEW_TYPE_ITEM:
                view = LayoutInflater
                        .from(parent.getContext()).inflate(R.layout.rv_item_bookmark, parent, false);
                holder = new ListBookmarkVH(view);
                break;
            default:
                holder = null;
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (position == 0) {
            if (mListBookmarks.isEmpty()) {
                ((HeaderBookmarkVH) holder).emptyMsg.setVisibility(View.VISIBLE);
            } else {
                ((HeaderBookmarkVH) holder).emptyMsg.setVisibility(View.GONE);
            }
            return;
        }

        ListBookmarkVH bookmarkVH = (ListBookmarkVH) holder;
        final KamusPenyakit penyakit = mListBookmarks.get(position - 1);

        DrawableCompat.setTint(bookmarkVH.bookmarkIcon.getDrawable(), colorGenerator.getRandomColor());

        bookmarkVH.kodeTv.setText(penyakit.kodePenyakit);
        bookmarkVH.namaTv.setText(penyakit.namaPenyakit);
        bookmarkVH.penjelasanTv.setText(penyakit.penjelasan);

        String date = new SimpleDateFormat("EEEE, d MMM ''yy").format(new Date(penyakit.bookmark));
        bookmarkVH.bookmarkDateTv.setText(date);

        bookmarkVH.closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) {
                    mClickListener
                            .onRecyclerSubItemClick(penyakit, PenyakitRvOnItemClickListener.SUB_ITEM_MODE_EDIT);
                        mListBookmarks.remove(penyakit);
                        notifyItemRemoved(holder.getAdapterPosition());
                }

            }
        });
        bookmarkVH.moreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) mClickListener
                        .onRecyclerSubItemClick(penyakit, PenyakitRvOnItemClickListener.SUB_ITEM_MODE_DETAIL);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListBookmarks.size() + 1;
    }

    public void refreshList(ArrayList<KamusPenyakit> newLists) {
        mListBookmarks = newLists;
        sortItemsBy(0);
        notifyDataSetChanged();
    }

    private void sortItemsBy(int order) {
        if (order == 0) {
            Collections.sort(mListBookmarks, new Comparator<KamusPenyakit>() {
                @Override
                public int compare(KamusPenyakit o1, KamusPenyakit o2) {
                    Date o1Date = new Date(o1.bookmark), o2Date = new Date(o2.bookmark);
                    return o2Date.compareTo(o1Date);
                }
            });
        } else if (order == 1) {
            Collections.sort(mListBookmarks, new Comparator<KamusPenyakit>() {
                @Override
                public int compare(KamusPenyakit o1, KamusPenyakit o2) {
                    Date o1Date = new Date(o1.bookmark), o2Date = new Date(o2.bookmark);
                    return o1Date.compareTo(o2Date);
                }
            });
        } else if (order == 2) {
            Collections.sort(mListBookmarks, new Comparator<KamusPenyakit>() {
                @Override
                public int compare(KamusPenyakit o1, KamusPenyakit o2) {
                    return o1.namaPenyakit.compareTo(o2.namaPenyakit);
                }
            });
        } else if (order == 3) {
            Collections.sort(mListBookmarks, new Comparator<KamusPenyakit>() {
                @Override
                public int compare(KamusPenyakit o1, KamusPenyakit o2) {
                    return o2.namaPenyakit.compareTo(o1.namaPenyakit);
                }
            });
        }
        notifyDataSetChanged();
    }

    private class HeaderBookmarkVH extends RecyclerView.ViewHolder {

        Spinner sortSpinner;
        TextView emptyMsg;

        HeaderBookmarkVH(View itemView) {
            super(itemView);
            sortSpinner = (Spinner) itemView.findViewById(R.id.rv_header_item_bookmark_spinner);
            emptyMsg = (TextView) itemView.findViewById(R.id.rv_header_item_msg_empty_bookmark);

            sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    sortItemsBy(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private class ListBookmarkVH extends RecyclerView.ViewHolder {

        ImageView bookmarkIcon;
        TextView namaTv, kodeTv, penjelasanTv, bookmarkDateTv;
        ImageButton closeBtn, moreBtn;

        ListBookmarkVH(View itemView) {
            super(itemView);
            bookmarkIcon = (ImageView) itemView.findViewById(R.id.rv_item_bookmark_iv);
            bookmarkDateTv = (TextView) itemView.findViewById(R.id.rv_item_bookmark_date_tv);
            kodeTv = (TextView) itemView.findViewById(R.id.rv_item_bookmark_subtitle);
            namaTv = (TextView) itemView.findViewById(R.id.rv_item_bookmark_title);
            penjelasanTv = (TextView) itemView.findViewById(R.id.rv_item_bookmark_content);
            closeBtn = (ImageButton) itemView.findViewById(R.id.rv_item_bookmark_close_btn);
            moreBtn = (ImageButton) itemView.findViewById(R.id.rv_item_bookmark_more_btn);

            itemView.setPadding(8, 0, 8, 8);
        }
    }
}
