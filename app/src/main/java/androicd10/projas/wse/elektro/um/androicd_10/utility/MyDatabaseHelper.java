package androicd10.projas.wse.elektro.um.androicd_10.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.util.LongSparseArray;
import android.util.SparseIntArray;
import android.util.SparseLongArray;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import androicd10.projas.wse.elektro.um.androicd_10.pojo.IndeksPenyakit;
import androicd10.projas.wse.elektro.um.androicd_10.pojo.KamusPenyakit;

/**
 * Created by DERI on 20/11/2017.
 */

public class MyDatabaseHelper extends SQLiteAssetHelper {

    // Database Info
    private static final String DATABASE_NAME = "androicd.db";
    private static final int DATABASE_VERSION = 1;

    // Table Names
    private static final String TABLE_PENYAKIT = "kamusPenyakit";
    private static final String TABLE_USER = "users";
    private static final String TABLE_BOOKMARK = "bookmarks";

    // Penyakit Table Columns
    private static final String COLUMN_PENYAKIT_ID = "penyakitId";
    private static final String COLUMN_PENYAKIT_NAMA = "nama";
    private static final String COLUMN_PENYAKIT_KODE = "kode";
    private static final String COLUMN_PENYAKIT_PENJELASAN = "penjelasan";
    private static final String COLUMN_PENYAKIT_PROPERTI = "properti";

    // User Table Columns
    private static final String COLUMN_USER_ID = "userId";
    private static final String COLUMN_USER_NAME = "userName";
    private static final String COLUMN_USER_PASSWORD = "userPassword";

    // Bookmark Table Columns
    private static final String COLUMN_BOOKMARK_ID = "id";
    private static final String COLUMN_BOOKMARK_USER_ID = "userId";
    private static final String COLUMN_BOOKMARK_PENYAKIT_ID = "penyakitId";
    private static final String COLUMN_BOOKMARK_TIMESTAMP = "timestamp";

    private final String[][] INDEKS_ICD = new String[][] {
        {"A00", "B99.9"}, {"C00", "D48.9"}, {"D50", "D89.9"}, {"E00", "E90.9"}, {"F00", "F99.9"},
        {"G00", "G99.9"}, {"H00", "H59.9"}, {"H60", "H95.9"}, {"I00", "I99.9"}, {"J00", "J99.9"},
        {"K00", "K93.9"}, {"L00", "L99.9"}, {"M00", "M99.9"}, {"N00", "N99.9"}, {"O00", "O99.9"},
        {"P00", "P96.9"}, {"Q00", "Q99.9"}, {"R00", "R99.9"}, {"S00", "T98.9"}, {"V01", "Y98.9"},
        {"Z00", "Z99.9"}
    };

    private static MyDatabaseHelper sInstance;
    private static ArrayList<KamusPenyakit> mListPenyakit;
    private static ArrayList<Integer> mListBookmark;
    private static ArrayList<IndeksPenyakit> mIndeksNamaPenyakit, mIndeksKodePenyakit;
    private LongSparseArray<Long> mBookmarksId;

    public static synchronized MyDatabaseHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new MyDatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    private MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void initializeDataBase() {
        getBookmarksData();
        readDataFromDb();
        initIndeksNama();
        initIndeksKode();
    }

    public ArrayList<KamusPenyakit> getListPenyakit() {
        if (mListPenyakit == null) {
            readDataFromDb();
        }
        return mListPenyakit;
    }

    public ArrayList<IndeksPenyakit> getIndeksNamaPenyakit() {
        if (mListPenyakit == null) readDataFromDb();
        if (mIndeksNamaPenyakit == null) initIndeksNama();
        return mIndeksNamaPenyakit;
    }

    public ArrayList<IndeksPenyakit> getIndeksKodePenyakit() {
        if (mListPenyakit == null) readDataFromDb();
        if (mIndeksKodePenyakit == null) initIndeksKode();
        return mIndeksKodePenyakit;
    }

    public ArrayList<Integer> getListBookmark() {
        if (mListBookmark == null) {
            readDataFromDb();
        }
        return mListBookmark;
    }

    public void upsertPenyakit(KamusPenyakit penyakit) {
        // The database connection is cached so it's not expensive to call getWriteableDatabase() multiple times.
        SQLiteDatabase db = getWritableDatabase();

        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_PENYAKIT_KODE, penyakit.kodePenyakit);
            values.put(COLUMN_PENYAKIT_NAMA, penyakit.namaPenyakit);
            values.put(COLUMN_PENYAKIT_PENJELASAN, penyakit.penjelasan);
            values.put(COLUMN_PENYAKIT_PROPERTI, 1);

            // First try to update the user in case the user already exists in the database
            // This assumes userNames are unique
            int rows = db.update(TABLE_PENYAKIT, values, COLUMN_PENYAKIT_ID + "= ?",
                    new String[]{penyakit.idPenyakit + ""});

            // Check if update succeeded
            if (rows == 1) {
                addNewChilds(penyakit);
                db.setTransactionSuccessful();
            }
            else {
                // user with this userName did not already exist, so insert new user
                penyakit.idPenyakit = db.insertOrThrow(TABLE_PENYAKIT, null, values);
                mListPenyakit.add(penyakit);
                addNewChilds(penyakit);
                db.setTransactionSuccessful();
            }
        } catch (Exception e) {
            Log.d("MyDatabaseHelper", "Error while trying to add or update user");
        } finally {
            db.endTransaction();
        }
    }

    public void addBookmark(KamusPenyakit penyakit) {
        SQLiteDatabase db = getWritableDatabase();

        // It's a good idea to wrap our insert in a transaction. This helps with performance and ensures
        // consistency of the database.
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_BOOKMARK_USER_ID, 1);
            values.put(COLUMN_BOOKMARK_PENYAKIT_ID, penyakit.idPenyakit);
            values.put(COLUMN_BOOKMARK_TIMESTAMP, penyakit.bookmark);

            // Notice how we haven't specified the primary key. SQLite auto increments the primary key column.
            db.insertOrThrow(TABLE_BOOKMARK, null, values);
            db.setTransactionSuccessful();
            mListBookmark.add(mListPenyakit.indexOf(penyakit));
            String date = new SimpleDateFormat("EEEE, d MMM ''yy").format(new Date(penyakit.bookmark));
            Log.d("MyDatabaseHelper", "*** Bookmark inserted: "+penyakit.idPenyakit
                    +" # "+penyakit.namaPenyakit+" # "+penyakit.kodePenyakit+" # "+penyakit.bookmark+"->"+date+" ***");
        } catch (Exception e) {
            Log.d("MyDatabaseHelper", "Error while trying to add bookmark to database");
        } finally {
            db.endTransaction();
        }
    }

    public void removeBookmark(KamusPenyakit penyakit) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            // Order of deletions is important when foreign key relationships exist.
            db.delete(TABLE_BOOKMARK, COLUMN_BOOKMARK_PENYAKIT_ID + " = ?",
                    new String[]{penyakit.idPenyakit + ""});
            db.setTransactionSuccessful();
            mListBookmark.remove(Integer.valueOf(mListPenyakit.indexOf(penyakit)));
            Log.d("MyDatabaseHelper", "*** Bookmark removed: "+penyakit.idPenyakit
                    +" # "+penyakit.namaPenyakit+" # "+penyakit.kodePenyakit+" ***");
        } catch (Exception e) {
            Log.d("MyDatabaseHelper", "Error while trying to delete all posts and users");
        } finally {
            db.endTransaction();
        }
    }

    private void readDataFromDb() {
        mListPenyakit = new ArrayList<>();
        if (mBookmarksId == null) getBookmarksData();

        SQLiteDatabase db = getReadableDatabase();
        String PENYAKIT_SELECT_QUERY = String.format("SELECT * FROM %s ORDER BY %s ASC",
                TABLE_PENYAKIT, COLUMN_PENYAKIT_NAMA);

        Cursor cursor = db.rawQuery(PENYAKIT_SELECT_QUERY, null);
        try {
            if (cursor.moveToFirst()) {
                mListBookmark = new ArrayList<>();
                int index = 0;
                do {
                    KamusPenyakit penyakit = new KamusPenyakit();
                    penyakit.idPenyakit = cursor.getLong(0);
                    penyakit.namaPenyakit = cursor.getString(cursor.getColumnIndex(COLUMN_PENYAKIT_NAMA));
                    penyakit.kodePenyakit = cursor.getString(cursor.getColumnIndex(COLUMN_PENYAKIT_KODE));
                    penyakit.penjelasan = cursor.getString(cursor.getColumnIndex(COLUMN_PENYAKIT_PENJELASAN));
                    penyakit.bookmark = mBookmarksId.get(penyakit.idPenyakit, 0L);

                    if (penyakit.isBookmarked()) mListBookmark.add(index);
                    mListPenyakit.add(index, penyakit); index++;

                    Log.d("MyDatabaseHelper", "*** List Penyakit added at "+index+": "+penyakit.idPenyakit+" # "+penyakit.namaPenyakit
                            +" # "+penyakit.kodePenyakit+" # "+penyakit.bookmark+" # ");
                } while(cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d("MyDatabaseHelper", "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
    }

    private void getBookmarksData() {
        if (mBookmarksId == null) mBookmarksId = new LongSparseArray<>();

        SQLiteDatabase db = getReadableDatabase();
        String BOOKMARK_SELECT_QUERY = String.format("SELECT * FROM %s", TABLE_BOOKMARK);
        Cursor cursor = db.rawQuery(BOOKMARK_SELECT_QUERY, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    long penyakitId = cursor.getLong(cursor
                            .getColumnIndex(COLUMN_BOOKMARK_PENYAKIT_ID));
                    long timestamp = cursor.getLong(cursor
                                    .getColumnIndex(COLUMN_BOOKMARK_TIMESTAMP));
                    mBookmarksId.put(penyakitId, timestamp);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d("MyDatabaseHelper", "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
    }

    private void initIndeksNama() {
        mIndeksNamaPenyakit = new ArrayList<>();
        // mIndeksNamaPenyakit.clear();
        String abjad = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (char c : abjad.toCharArray()) {
            IndeksPenyakit penyakit = new IndeksPenyakit(c+"");
            mIndeksNamaPenyakit.add(penyakit);
        }

        for (KamusPenyakit kp : mListPenyakit) {
            String kpOrder = kp.namaPenyakit.charAt(0)+"";
            mIndeksNamaPenyakit.get(abjad.indexOf(kpOrder.toUpperCase())).addChild(mListPenyakit.indexOf(kp));
        }
    }

    private void initIndeksKode() {
        mIndeksKodePenyakit = new ArrayList<>();
        //mIndeksKodePenyakit.clear();
        for (int i=0; i<INDEKS_ICD.length; i++) {
            IndeksPenyakit ip = new IndeksPenyakit("Kode "+INDEKS_ICD[i][0]+" - "+INDEKS_ICD[i][1]);
            mIndeksKodePenyakit.add(ip);
        }

        for (KamusPenyakit kp : mListPenyakit) {
            String kpKode = kp.kodePenyakit;
            IndeksPenyakit ip = mIndeksKodePenyakit.get(findIndeksByKode(kpKode));
            int index = 0;
            for (Integer i : ip.getChilds()) {
                if (mListPenyakit.get(i).kodePenyakit.compareTo(kpKode) > 0) break;
                index++;
            }

            ip.addChild(mListPenyakit.indexOf(kp), index);
        }
    }

    private void addNewChilds(KamusPenyakit kp) {
        // Indeks nama
        String abjad = "ABCDEFGHIJKLMNOPQRSTUVWXYZ", kpOrder = kp.namaPenyakit.charAt(0)+"";
        Integer integer = mListPenyakit.indexOf(kp);
        mIndeksNamaPenyakit.get(abjad.indexOf(kpOrder)).removeChild(integer);
        mIndeksNamaPenyakit.get(abjad.indexOf(kpOrder)).addChild(mListPenyakit.indexOf(kp));

        // Indeks kode
        String kpKode = kp.kodePenyakit;
        IndeksPenyakit ip = mIndeksKodePenyakit.get(findIndeksByKode(kpKode));
        int index = 0;
        for (Integer i : ip.getChilds()) {
            if (mListPenyakit.get(i).kodePenyakit.compareTo(kpKode) > 0) break;
            index++;
        }
        ip.removeChild(integer);
        ip.addChild(mListPenyakit.indexOf(kp), index);
    }

    private int findIndeksByKode(String kode) {
        for (int i=0; i<INDEKS_ICD.length; i++) {
            if ((kode.compareTo(INDEKS_ICD[i][0]) >= 0 && kode.compareTo(INDEKS_ICD[i][1]) < 0) ||
                    (kode.compareTo(INDEKS_ICD[i][0]) > 0 && kode.compareTo(INDEKS_ICD[i][1]) <= 0))
                return i;
        } return INDEKS_ICD.length - 1;
    }


}
