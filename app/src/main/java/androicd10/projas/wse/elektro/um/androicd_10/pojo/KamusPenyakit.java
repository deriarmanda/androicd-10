package androicd10.projas.wse.elektro.um.androicd_10.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import androicd10.projas.wse.elektro.um.androicd_10.utility.MyDatabaseHelper;

/**
 * Created by DERI on 16/11/2017.
 */

public class KamusPenyakit implements Parcelable {
    public long idPenyakit;
    public String namaPenyakit;
    public String kodePenyakit;
    public String penjelasan;
    public long bookmark;

    public KamusPenyakit() {
        bookmark = 0;
    }

    public KamusPenyakit(String namaPenyakit, String kodePenyakit, String penjelasan, long bookmark) {
        this.namaPenyakit = namaPenyakit;
        this.kodePenyakit = kodePenyakit;
        this.penjelasan = penjelasan;
        this.bookmark = bookmark;
    }

    public boolean isBookmarked() {
        return bookmark != 0;
    }

    protected KamusPenyakit(Parcel in) {
        idPenyakit = in.readInt();
        namaPenyakit = in.readString();
        kodePenyakit = in.readString();
        penjelasan = in.readString();
        bookmark = in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(idPenyakit);
        dest.writeString(namaPenyakit);
        dest.writeString(kodePenyakit);
        dest.writeString(penjelasan);
        dest.writeLong(bookmark);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<KamusPenyakit> CREATOR = new Parcelable.Creator<KamusPenyakit>() {
        @Override
        public KamusPenyakit createFromParcel(Parcel in) {
            return new KamusPenyakit(in);
        }

        @Override
        public KamusPenyakit[] newArray(int size) {
            return new KamusPenyakit[size];
        }
    };
}