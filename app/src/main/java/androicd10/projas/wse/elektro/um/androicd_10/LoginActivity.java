package androicd10.projas.wse.elektro.um.androicd_10;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText etUsername = (EditText) findViewById(R.id.login_et_username);
        Button btLogin = (Button) findViewById(R.id.login_bt_login);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("AICD_Login", 0);
        final SharedPreferences.Editor editor = pref.edit();

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = etUsername.getText().toString();
                if (username.length() < 8) {
                    Toast.makeText(LoginActivity.this, "Maaf, username harus lebih dari 8 huruf !",
                            Toast.LENGTH_SHORT).show();
                } else {
                    editor.putString("key_username", username);
                    editor.apply();
                    Intent data = new Intent();
                    data.putExtra("username", username);
                    setResult(RESULT_OK, data); finish();
                }
            }
        });
    }
}
