package androicd10.projas.wse.elektro.um.androicd_10;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import androicd10.projas.wse.elektro.um.androicd_10.pojo.KamusPenyakit;
import androicd10.projas.wse.elektro.um.androicd_10.utility.MyDatabaseHelper;

public class DetailActivity extends AppCompatActivity {

    private TextView tvNama, tvKode, tvPenjelasan;
    private ImageButton btnBookmark;

    private KamusPenyakit penyakit;
    private MyDatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        databaseHelper = MyDatabaseHelper.getInstance(this);

        tvKode = (TextView) findViewById(R.id.detail_tv_kode);
        tvPenjelasan = (TextView) findViewById(R.id.detail_tv_penjelasan);
        tvNama = (TextView) findViewById(R.id.detail_tv_nama);
        tvNama.setSelected(true);

        ImageButton btnBack = (ImageButton) findViewById(R.id.detail_btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnBookmark = (ImageButton) findViewById(R.id.detail_btn_bookmark);
        btnBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (penyakit.isBookmarked()) {
                    databaseHelper.removeBookmark(penyakit);
                    penyakit.bookmark = 0;
                    btnBookmark.animate().rotation(-360).start();
                    btnBookmark.setImageResource(R.drawable.ic_bookmark_border_accent_24dp);
                    Toast.makeText(DetailActivity.this, penyakit.namaPenyakit
                            + " dihapus dari bookmark.", Toast.LENGTH_SHORT).show();
                } else {
                    penyakit.bookmark = new Date().getTime();
                    databaseHelper.addBookmark(penyakit);
                    btnBookmark.animate().rotation(360).start();
                    btnBookmark.setImageResource(R.drawable.ic_bookmark_accent_24dp);
                    Toast.makeText(DetailActivity.this, penyakit.namaPenyakit
                            + " ditambahkan ke bookmark.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        FloatingActionButton fabEdit = (FloatingActionButton) findViewById(R.id.detail_fab_edit);
        fabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editIntent = new Intent(DetailActivity.this, UpsertActivity.class);
                editIntent.putExtra("indeks_list", databaseHelper.getListPenyakit().indexOf(penyakit));
                startActivity(editIntent);
            }
        });
        fabEdit.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Snackbar.make(v, "Ubah data kamus penyakit.", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        int index = getIntent().getIntExtra("indeks_list", 0);
        penyakit = databaseHelper.getListPenyakit().get(index);
        tvNama.setText(penyakit.namaPenyakit);
        String kode = "Kode " + penyakit.kodePenyakit;
        tvKode.setText(kode);
        tvPenjelasan.setText(penyakit.penjelasan);

        int bookmarkRes = penyakit.isBookmarked()? R.drawable.ic_bookmark_accent_24dp
                : R.drawable.ic_bookmark_border_accent_24dp;
        btnBookmark.setImageResource(bookmarkRes);
        tvNama.setSelected(true);
    }
}
