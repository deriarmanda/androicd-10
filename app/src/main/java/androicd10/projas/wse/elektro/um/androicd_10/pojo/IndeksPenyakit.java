package androicd10.projas.wse.elektro.um.androicd_10.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by DERI on 16/11/2017.
 */

public class IndeksPenyakit implements Parcelable {
    private String indeksTitle;
    private ArrayList<Integer> childs;

    public IndeksPenyakit(String indeksTitle) {
        this.indeksTitle = indeksTitle;
        childs = new ArrayList<>();
    }

    public IndeksPenyakit(String indeksTitle, ArrayList<Integer> childs) {
        this.indeksTitle = indeksTitle;
        this.childs = childs;
    }

    public String getIndeksTitle() {
        return indeksTitle;
    }

    public ArrayList<Integer> getChilds() {
        return childs;
    }

    public int getChildCount() {
        return childs.size();
    }

    public boolean addChild(Integer newChild) {
        return childs.add(newChild);
    }

    public void addChild(Integer newChild, int index) {
        childs.add(index, newChild);
    }

    public boolean removeChild(Integer oldChild) {
        return childs.remove(oldChild);
    }

    private IndeksPenyakit(Parcel in) {
        indeksTitle = in.readString();
        if (in.readByte() == 0x01) {
            childs = new ArrayList<Integer>();
            in.readList(childs, KamusPenyakit.class.getClassLoader());
        } else {
            childs = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(indeksTitle);
        if (childs == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(childs);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<IndeksPenyakit> CREATOR = new Parcelable.Creator<IndeksPenyakit>() {
        @Override
        public IndeksPenyakit createFromParcel(Parcel in) {
            return new IndeksPenyakit(in);
        }

        @Override
        public IndeksPenyakit[] newArray(int size) {
            return new IndeksPenyakit[size];
        }
    };
}