package androicd10.projas.wse.elektro.um.androicd_10;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

import androicd10.projas.wse.elektro.um.androicd_10.pojo.IndeksPenyakit;
import androicd10.projas.wse.elektro.um.androicd_10.pojo.KamusPenyakit;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.ListPenyakitRvAdapter;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.PenyakitRvItemDecoration;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.PenyakitRvOnItemClickListener;
import androicd10.projas.wse.elektro.um.androicd_10.utility.MyDatabaseHelper;

public class ListPenyakitActivity extends AppCompatActivity {

    RecyclerView rvListPenyakit;
    MaterialSearchView searchView;
    LinearLayoutManager rvListLayoutManager;
    ListPenyakitRvAdapter rvListAdapter;
    PenyakitRvOnItemClickListener rvOnItemClickListener;

    IndeksPenyakit indeksPenyakit;
    MyDatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_penyakit);

        databaseHelper = MyDatabaseHelper.getInstance(this);
        indeksPenyakit = getIntent().getParcelableExtra("indeks_penyakit");
        String title = indeksPenyakit.getIndeksTitle();
        if (title.length() == 1) title = "List Abjad "+title;
        Toolbar toolbar = (Toolbar) findViewById(R.id.list_penyakit_toolbar);
        toolbar.setTitle(title); setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        List<KamusPenyakit> listPenyakit = new ArrayList<>();
        final List<KamusPenyakit> listPenyakitCopy = new ArrayList<>();
        for (Integer i : indeksPenyakit.getChilds()) {
            KamusPenyakit kp = databaseHelper.getListPenyakit().get(i);
            listPenyakit.add(kp);
            listPenyakitCopy.add(kp);
        }

        rvOnItemClickListener = new PenyakitRvOnItemClickListener() {
            @Override
            public void onRecyclerItemClick(IndeksPenyakit indeksPenyakit) {

            }

            @Override
            public void onRecyclerSubItemClick(KamusPenyakit kamusPenyakit, int clickMode) {
                switch (clickMode) {
                    case PenyakitRvOnItemClickListener.SUB_ITEM_MODE_DETAIL:
                        Intent detailIntent = new Intent(ListPenyakitActivity.this, DetailActivity.class);
                        detailIntent.putExtra("indeks_list", databaseHelper.getListPenyakit().indexOf(kamusPenyakit));
                        startActivity(detailIntent);
                        break;
                    case PenyakitRvOnItemClickListener.SUB_ITEM_MODE_EDIT:
                        Intent editIntent = new Intent(ListPenyakitActivity.this, UpsertActivity.class);
                        editIntent.putExtra("indeks_list", databaseHelper.getListPenyakit().indexOf(kamusPenyakit));
                        startActivity(editIntent);
                        break;
                    case PenyakitRvOnItemClickListener.SUB_ITEM_MODE_BOOKMARK:
                        if (kamusPenyakit.bookmark == 0) {
                            databaseHelper.removeBookmark(kamusPenyakit);
                            Toast.makeText(ListPenyakitActivity.this, kamusPenyakit.namaPenyakit
                                    + " dihapus dari bookmark.", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            databaseHelper.addBookmark(kamusPenyakit);
                            Toast.makeText(ListPenyakitActivity.this, kamusPenyakit.namaPenyakit
                                    + " ditambahkan ke bookmark.", Toast.LENGTH_SHORT).show();
                        }

                        Log.d("ListPenyakitActivity", "*** "+kamusPenyakit.idPenyakit+" # "+kamusPenyakit.namaPenyakit
                                +" # "+kamusPenyakit.kodePenyakit+" # "+kamusPenyakit.bookmark+" # ");
                        break;
                    default:
                        break;
                }
            }
        };
        rvListAdapter = new ListPenyakitRvAdapter(listPenyakit, rvOnItemClickListener);
        rvListPenyakit = (RecyclerView) findViewById(R.id.list_penyakit_rv);
        rvListPenyakit.setAdapter(rvListAdapter);

        rvListLayoutManager = new LinearLayoutManager(ListPenyakitActivity.this);
        rvListPenyakit.setLayoutManager(rvListLayoutManager);

        searchView = (MaterialSearchView) findViewById(R.id.list_penyakit_search_view);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do some magic
                rvListAdapter.onQuerySearch(listPenyakitCopy, query, 1, false);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                rvListAdapter.onQuerySearch(listPenyakitCopy, newText, 1, false);
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        rvListAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.list_penyakit_search_action);
        searchView.setMenuItem(item);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) searchView.closeSearch();
        else super.onBackPressed();
    }
}
