package androicd10.projas.wse.elektro.um.androicd_10;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import androicd10.projas.wse.elektro.um.androicd_10.utility.MyDatabaseHelper;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        new LoadDatabase().execute();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    private class LoadDatabase extends AsyncTask<Context, Integer, String> {

        @Override
        protected String doInBackground(Context... params) {
            MyDatabaseHelper.getInstance(SplashActivity.this).initializeDataBase();
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);

            finish();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            finish();
        }
    }
}
