package androicd10.projas.wse.elektro.um.androicd_10;

import android.content.DialogInterface;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import androicd10.projas.wse.elektro.um.androicd_10.pojo.KamusPenyakit;
import androicd10.projas.wse.elektro.um.androicd_10.utility.MyDatabaseHelper;

public class UpsertActivity extends AppCompatActivity {

    KamusPenyakit penyakit;
    MyDatabaseHelper databaseHelper;

    TextInputEditText fieldKode, fieldNama;
    EditText fieldDeskripsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upsert);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        databaseHelper = MyDatabaseHelper.getInstance(this);

        fieldNama = (TextInputEditText) findViewById(R.id.upsert_nama_field);
        fieldKode = (TextInputEditText) findViewById(R.id.upsert_kode_field);
        fieldDeskripsi = (EditText) findViewById(R.id.upsert_deskripsi_field);

    }

    @Override
    protected void onResume() {
        super.onResume();

        int indeks = getIntent().getIntExtra("indeks_list", -1);
        if (indeks >=0 ) {
            penyakit = databaseHelper.getListPenyakit().get(indeks);
            fieldKode.setText(penyakit.kodePenyakit);
            fieldNama.setText(penyakit.namaPenyakit);
            fieldDeskripsi.setText(penyakit.penjelasan);
            Log.d("UpsertActivity", "Edit Penyakit");
        }
        else penyakit = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_upsert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.upsert_simpan_menu) {
            upsertData();
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Kembali ke menu awal? Perubahan data akan hilang.");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ya",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        UpsertActivity.super.onBackPressed();
                    }
                });

        builder1.setNegativeButton(
                "Tidak",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void upsertData() {
        // Get new data
        String nama = fieldNama.getText().toString(),
                kode = fieldKode.getText().toString(),
                deskripsi = fieldDeskripsi.getText().toString();

        // Form Validation
        if (kode.isEmpty() || nama.isEmpty()) {
            final AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("Kode dan Nama penyakit tidak boleh kosong !");
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "Oke",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
            return;
        }

        String msg;
        if (penyakit != null) {
            penyakit.namaPenyakit = nama;
            penyakit.kodePenyakit = kode;
            penyakit.penjelasan = deskripsi;

            msg = "Berhasil merubah " + nama;
        } else {
            penyakit = new KamusPenyakit(nama, kode, deskripsi, 0);
            msg = "Penyakit " + nama + " ditambahkan ke kamus.";
        }
        databaseHelper.upsertPenyakit(penyakit);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        finish();
    }
}
