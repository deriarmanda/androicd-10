package androicd10.projas.wse.elektro.um.androicd_10;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.facebook.stetho.Stetho;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import androicd10.projas.wse.elektro.um.androicd_10.fragments.TabFragmentAdapter;
import androicd10.projas.wse.elektro.um.androicd_10.recyclerviews.PenyakitRvOnScrollListener;

public class MainActivity extends AppCompatActivity {

    public static final int RC_SIGN_IN = 91;

    // Utility Variable
    SharedPreferences pref;
    String mainTabTitle[], username;
    PenyakitRvOnScrollListener namaRvScrollListener;
    boolean isSignedIn;

    // Material Design Variable
    TabLayout mainTab;
    ViewPager mainViewPager;
    TabFragmentAdapter mainTabAdapter;
    MaterialSearchView mainSearchView;
    FloatingActionButton mainAddFab;

    // View Variable

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Stetho.initializeWithDefaults(this);

        pref = getApplicationContext().getSharedPreferences("AICD_Login", 0);
        checkLogin();
        setupToolbar();
        setupFab();
        prepareViews();

        if (isSignedIn) {
            Snackbar.make(mainAddFab, "Selamat datang kembali, " + username + " :)",
                    Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainActivity.RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                username = data.getStringExtra("username");
                Snackbar.make(mainAddFab, "Selamat datang " + username + " :)",
                        Snackbar.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Sampai Jumpa :)", Toast.LENGTH_SHORT).show();
                finish();
            }
        } else super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mainSearchView.isSearchOpen()) mainSearchView.closeSearch();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.main_action_search);
        mainSearchView.setMenuItem(item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.main_action_search:
//                Intent searchIntent = new Intent(this, SearchActivity.class);
//                startActivity(searchIntent);
//                Toast.makeText(this, "search clicked", Toast.LENGTH_LONG).show();
                break;
            case R.id.main_action_sign_out:
                SharedPreferences.Editor editor = pref.edit();
                editor.clear(); editor.apply();
                checkLogin();
                break;
            default:
                break;
        } return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mainSearchView.isSearchOpen()) {
            mainSearchView.closeSearch(); return;
        }
        if (mainTab.getSelectedTabPosition() != 0) {
            mainViewPager.setCurrentItem(0);
            mainTab.setScrollPosition(0, 0f, true); return;
        }
        super.onBackPressed();
    }

    private void checkLogin() {
        isSignedIn = false;

        username = pref.getString("key_username", "-");
        isSignedIn = !(username.equals("-"));

        if (!isSignedIn) {
            startActivityForResult(new Intent(this, LoginActivity.class), MainActivity.RC_SIGN_IN);
        }
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    private void setupFab() {
        // FAB
        mainAddFab = (FloatingActionButton) findViewById(R.id.main_add_fab);

        mainAddFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editIntent = new Intent(MainActivity.this, UpsertActivity.class);
                startActivity(editIntent);
            }
        });
        mainAddFab.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//                Toast toast = Toast.makeText(MainActivity.this, "Add new dictionary..", Toast.LENGTH_SHORT);
//                toast.setGravity(Gravity.BOTTOM, 0, 24); toast.show();
                Snackbar.make(v, "Tambah kamus baru.",Snackbar.LENGTH_SHORT)
                        .show();
                return true;
            }
        });

        // Scroll Change Listener
        namaRvScrollListener = new PenyakitRvOnScrollListener() {
            @Override
            public void onRecyclerViewScrollStateChanged(RecyclerView recyclerView, int state) {
                if (state != RecyclerView.SCROLL_STATE_SETTLING) mainAddFab.show();
                else mainAddFab.hide();
            }
        };
    }

    private void prepareViews() {
        // Tab Fragments
        mainTabTitle = getResources().getStringArray(R.array.main_tab_title);
        mainTabAdapter = new TabFragmentAdapter(getSupportFragmentManager(), namaRvScrollListener, mainTabTitle);
        mainViewPager = (ViewPager) findViewById(R.id.main_view_pager);
        mainViewPager.setAdapter(mainTabAdapter);
        mainTab = (TabLayout) findViewById(R.id.main_tab_layout);
        mainTab.setupWithViewPager(mainViewPager);

        // Material Search View
        mainSearchView = (MaterialSearchView) findViewById(R.id.main_search_button);
        mainSearchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                mainAddFab.hide();
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
            }

            @Override
            public void onSearchViewClosed() {
                mainAddFab.show();
            }
        });

        // BottomSheet

    }
}
