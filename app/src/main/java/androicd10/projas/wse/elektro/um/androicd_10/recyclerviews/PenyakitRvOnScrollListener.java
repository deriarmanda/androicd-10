package androicd10.projas.wse.elektro.um.androicd_10.recyclerviews;

import android.support.v7.widget.RecyclerView;

/**
 * Created by DERI on 30/10/2017.
 */

public interface PenyakitRvOnScrollListener {
    void onRecyclerViewScrollStateChanged(RecyclerView recyclerView, int state);
}
