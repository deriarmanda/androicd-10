package androicd10.projas.wse.elektro.um.androicd_10.recyclerviews;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.List;

import androicd10.projas.wse.elektro.um.androicd_10.R;
import androicd10.projas.wse.elektro.um.androicd_10.pojo.IndeksPenyakit;

/**
 * Created by DERI on 19/11/2017.
 */

public class FragmentKodeRvAdapter extends RecyclerView.Adapter {

    private final ColorGenerator colorGenerator = ColorGenerator.MATERIAL;
    private final String[] romansNumber = {"I", "II", "III", "IV", "V", "VI", "VII",
            "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII",
            "XIX", "XX", "XXI", "XXII", "XXIII", "XXIV", "XXV"};

    private List<IndeksPenyakit> mListIndeksKode;
    private String[] mIndeksSubtitle;
    private PenyakitRvOnItemClickListener mClickListener;

    public FragmentKodeRvAdapter(List<IndeksPenyakit> mListIndeksKode, String[] mIndeksSubtitle,
                                 PenyakitRvOnItemClickListener mClickListener) {
        this.mListIndeksKode = mListIndeksKode;
        this.mIndeksSubtitle = mIndeksSubtitle;
        this.mClickListener = mClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item_kode_penyakit, parent, false);
        return new IndeksListVH(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final IndeksPenyakit curIndeks = mListIndeksKode.get(position);
        IndeksListVH indeksVH = (IndeksListVH) holder;

        indeksVH.indeksTitle.setText(curIndeks.getIndeksTitle());
        indeksVH.indeksSubtitle.setText(mIndeksSubtitle[position]);

        String titleDrawable = romansNumber[position];
        int color = colorGenerator.getColor(titleDrawable);
        TextDrawable drawable = TextDrawable.builder().buildRound(titleDrawable, color);
        indeksVH.indeksImage.setImageDrawable(drawable);

        indeksVH.indeksContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) mClickListener.onRecyclerItemClick(curIndeks);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mListIndeksKode.size();
    }

    private class IndeksListVH extends RecyclerView.ViewHolder {

        RelativeLayout indeksContainer;
        ImageView indeksImage;
        TextView indeksTitle, indeksSubtitle;

        IndeksListVH(View itemView) {
            super(itemView);
            indeksContainer = (RelativeLayout) itemView.findViewById(R.id.rv_item_kode_indeks_container);
            indeksImage = (ImageView) itemView.findViewById(R.id.rv_item_kode_indeks_image);
            indeksTitle = (TextView) itemView.findViewById(R.id.rv_item_kode_indeks_title);
            indeksSubtitle = (TextView) itemView.findViewById(R.id.rv_item_kode_indeks_subtitle);
        }
    }
}
